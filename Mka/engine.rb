class Engine

  def initialize(room_map)
    @room_map = room_map
  end

  def play()
    current_room = @room_map.opening_room()
    last_room = @room_map.next_room('fishing')

    while current_room != last_room
      next_room_name = current_room.enter()
      current_room = @room_map.next_room(next_room_name);
    end

    current_room.enter()
  end
end
