class Convefight < Room

  def enter()

    iconvfight
    print "> "

    action = $stdin.gets.chomp.downcase

    if action == "fight"
         fight_conv
      conv_fight = Puzz.new()
      conv_fight.time = 5
      conv_fight.prompt = "[Beatdown]> "
      conv_fight.dialog_1 =
      conv_fight.dialog_2 =
      conv_fight.dialog_3 =
      conv_fight.pass = pass_conv
      conv_fight.fail = fail_conv
      conv_fight.room_1 = 'fishing'
      conv_fight.room_2 = 'men_brkd'
      conv_fight.comb_room()
    elsif action == "think"
      think_conv
      return 'conv_fight'
    else
      puts "You need to [think] about the situation you are in right now."
      sleep 2
      return 'conv_fight'
    end

  end
end
