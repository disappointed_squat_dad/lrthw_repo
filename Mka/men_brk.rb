class Men_brk < Room
  @@mtl = [
    "You've given up all hope. The void slowly crushes you.",
     "You've simply lost all ability to function. Time to sleep.",
     "The feelings of guilt consume you. You don't know if you're cut out for this life.",
     "You slowly step back and start walking home. A single tear drops down your cheeck as you contemplate yourself.",
     "Pausing breifly you stop and think. Why did you do this... why this.... time to smoke a cig and take a nap."
          ]

  def enter()
    puts @@mtl[rand(0..(@@mtl.length - 1))]
    exit(1)
  end
end
