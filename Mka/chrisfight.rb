class Chrisfight < Room

  def enter()
    chris_fighti
    print "> "
    action = $stdin.gets.chomp.downcase

    if action == "fight"
      fight_cf
      chris_fight = Puzz.new()
      chris_fight.time = 7
      chris_fight.prompt = "[Ruckus]> "
      chris_fight.dialog_1 = cf_txt_1
      chris_fight.dialog_2 = cf_txt_2
      chris_fight.dialog_3 = cf_txt_3
      chris_fight.pass = pass_cf
      chris_fight.fail = fail_cf
      chris_fight.room_1 = 'conv'
      chris_fight.room_2 = 'men_brkd'
      chris_fight.comb_room()
    elsif action == "think"
      think_cf
      sleep 3
      return 'chris_fight'
    else
      puts "What the fuck are you doing, you should really"
      slp
      puts "[think] about the situation you're in right now."
      slp
      return 'chris_fight'
    end
  end
end
