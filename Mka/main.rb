%w(room house outside
   garage chrismar chrisfight
   conve convefight fishing
   men_brk map engine).each do |file|
    require_relative "./#{file}.rb"
end

#loads strings for choices at the beginning of the program
#Requires module Puzzle
Dir["./txt/*.rb"].each {|file| load file }



a_map = Map.new('house')
a_game = Engine.new(a_map)
puts "At any time when prompted by '>' you may type 'think' to check what actions are available."
a_game.play()
