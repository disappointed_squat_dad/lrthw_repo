class Map
  @@rooms = {
    'house' => House.new(),
    'outside' => Outside.new(),
    'garage' => Garage.new(),
    'chrismar' => Chrismar.new(),
    'chris_fight' => Chrisfight.new(),
    'conv' => Conve.new(),
    'conv_fight' => Convefight.new(),
    'fishing' => Fishing.new(),
    'men_brkd' => Men_brk.new(),
  }

  def initialize(start_room)
    @start_room = start_room
  end

  def next_room(room_name)
    val = @@rooms[room_name]
    return val
  end

  def opening_room()
    return next_room(@start_room)
  end
end
