def ifishing()
  slp
  puts "You stop at a gas station and pick up the 305 ruckus."
  slp
  puts "You decide to drive out to the lagoon. On your way you light up and "
  slp
  puts "combust the tobacco. The cool menthol laces your mouth as you exhale."
  slp
  puts "You get out of your truck and start vomiting. The thrill and rush "
  slp
  puts "of the sublime experiential bliss that comes with the mullet run"
  slp
  puts "is one which is unlike no other."
  slp
  puts "You grab your rod and reel and treck through the path."
  slp
  puts "You step off the path and dip your feet in the cool ocean."
  slp
  puts "You draw your arms back, face bloody, covered in sweat.... and cast"
  slp
  puts "off..."
  slp
  puts "As the morning sun rises you let out a cough and flem drops into the water"
  slp
  puts "floating with the flow of the tide."
  slp
  puts "You pause and think to yourself 'Just another day.'"
  slp
end
