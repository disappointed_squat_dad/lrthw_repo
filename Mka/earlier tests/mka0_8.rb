def slp()
  sleep 0.5
end

#loads strings for choices at the beginning of the program
#Requires module Puzzle
load 'houser.rb' 
load 'outsider.rb'
load 'garager.rb'
load 'chrismarr.rb'
load 'chrisfightr.rb'
load 'conver.rb'
load 'fishingr.rb'
require './Puzz.rb'

def insults()
  insult = [
    "You start trying to figure out what to do, but your brain starts hurting.",
      "You fuck up.... It's not the first time you fucked up.... so you fuck up again.",
      "You tried doing something and then got confused. You started thinking about the absolute and got lost in the void.",
      "You start to do something and then cough really hard. A huge ball of flem falls on your shirt and you lose motivation.",
      "You look around and scratch your buttcheeks and then smell your fingers. Great job!",
      "You started to do something, then thought about Hegel again. 'The owl of Minerva spreads its wings only with the falling of the dusk'... You start to get aroused but stop yourself from overthinking the nature of philosophy and its limits.",
      "You tried to do something, but then remembered that one time you pooped on the beach and started thinking about phenomenology.",
      "The ignorant man is not free, because what confronts him is an alien world, something outside him and in the offing, on which he depends, without his having made this foreign world for himself and therefore without being at home in it by himself as in something his own. The impulse of curiosity, the pressure for knowledge, from the lowest level up to the highest rung of philosophical insight arises only from the struggle to cancel this situation of unfreedom and to make the world one's own in one's ideas and thought."
  ]
  
  puts insult[rand(0..(insult.length - 1))]
  sleep 3
  exit(1)
end

def cigcount()
  cig = 0
end  

class Room
  def enter()
    puts "needs to be config"
    exit(1)
  end
end


class Engine
  
  def initialize(room_map)
    @room_map = room_map
  end
  
  def play()
    current_room = @room_map.opening_room()
    last_room = @room_map.next_room('fishing')
    
    while current_room != last_room
      next_room_name = current_room.enter()
      current_room = @room_map.next_room(next_room_name);
    end
    
    
    current_room.enter()
  end
end

class Men_brk < Room
  
  @@mtl = [
    "You've given up all hope. The void slowly crushes you.",
     "You've simply lost all ability to function. Time to sleep.",
     "The feelings of guilt consume you. You don't know if you're cut out for this life.",
     "You slowly step back and start walking home. A single tear drops down your cheeck as you contemplate yourself.",
     "Pausing breifly you stop and think. Why did you do this... why this.... time to smoke a cig and take a nap."
  ]
  
  def enter()
    puts @@mtl[rand(0..(@@mtl.length - 1))]
    exit(1)
  end
end

class House < Room

  
  
  def enter()
    sleep 5
    ihouse
    print "> "
    
    action = $stdin.gets.chomp.downcase
    
    if action == "cig"
      cigh
      @cigcount = 1
      return 'house'
      
    elsif action == "fight"
      fighth
      return 'house'
      
    elsif action == "weed"
      weedh
      return 'house'
      
    elsif action == "poop"
      pooph
      return 'house'
      
    elsif action == "go outside"
      
      if @cigcount == 1
        goh
        return 'outside'
        
      else
        puts "You can't head out without smoking your first cancer stick of the day"
        return 'house'
      end
      
    elsif action == "think"
      thinkh
      return 'house'
      
    elsif action == "talk"
      talkh
      return 'house'
      
    else
      insults
      return 'house'
      
    end 
  end
end

class Outside < Room

  
  
  def enter()
    
    cigcount
    
    ioutside()
    print "> "
    
    action = $stdin.gets.chomp
    
    if action == "cig" || "Cig"
      cigcount = 1
      cigo
      return 'outside'
    elsif action == "fight" || "Fight"
      fighto
      return 'outside'
    elsif action == "weed" || "Weed"
      weedo
      return 'outside'
    elsif action == "poop" || "Poop"
      poopo
      return 'outside'
    elsif action == "go to the garage" || "Go to the garage" || "Go to the Garage"
      goo
      return 'garage'
    elsif action == "think" || "Think"
      thinko
      return 'outside'
    elsif action == "talk" || "Talk"
      talko
      return 'outside'
    else
      insults
      return 'outside'
    end
  end
end

class Garage < Room
  
  def enter()
    
    igarage
    lock = Puzz.new()
    lock.time = 9
    lock.prompt = "[Dial]> "
    lock.dialog_1 = gtxt_1
    lock.dialog_2 = gtxt_2
    lock.dialog_3 = gtxt_3
    lock.pass = passg
    lock.fail = failg
    lock.room_1 = 'chrismar'
    lock.room_2 = 'men_brkd'
    lock.comb_room()
  end
end

class Chrismar < Room

  
  def enter()
    
    cigcount
    ichrismar
    print "> "
    
    action = $stdin.gets.chomp
    
    if action == "cig" || "Cig"
      cigc
      cigcount = 1
    elsif action == "fight" || "Fight"
      fightc
      return 'chrismar'
    elsif action == "weed" || "Weed"
      weedc
      return 'chrismar'
    elsif action == "poop" || "Poop"
      poopc
      return 'chrismar'
    elsif action == "go elsewhere" || "Go elsewhere" || "Go Elsewhere"
      goc
      return 'men_brkd'
    elsif action == "think" || "Think"
      return 'chrismar'
    elsif action == "talk" || "Talk"
      if cigcount == 1
        talkc
        return 'chris_fight'
      else
        puts "You don't feel like talking at the moment."
        sleep 5
        return 'chrismar'
      end
    else
      insults
      return 'chrismar'
    end
  end
end

class Chrisfight < Room
  
  
  def enter()
  
    chris_fighti
    print "> "
    action = $stdin.gets.chomp
  
    if action == "fight" || "Fight"
      
      fight_cf
      chris_fight = Puzz.new()
      chris_fight.time = 7
      chris_fight.prompt = "[Ruckus]> "
      chris_fight.dialog_1 = cf_txt_1
      chris_fight.dialog_2 = cf_txt_2
      chris_fight.dialog_3 = cf_txt_3
      chris_fight.pass = pass_cf
      chris_fight.fail = fail_cf
      chris_fight.room_1 = 'conv'
      chris_fight.room_2 = 'men_brkd'
      chris_fight.comb_room()
    elsif action == "think" || "Think"
      think_cf
      sleep 3
      return 'chris_fight'
    else
      puts "What the fuck are you doing, you should really"
      slp
      puts "[think] about the situation you're in right now."
      slp
      return 'chris_fight'
    end
  end
end

class Conve < Room
  
  
  def enter()
    
    iconve
    print "> "
    
    action = $stdin.gets.chomp
    
    if action == "cig" || "Cig"
      cig_conv
      return 'conv'
    elsif action == "fight" || "Fight"
      fight_conv
      return 'conv'
    elsif action == "weed" || "Weed"
      weed_conv
      return 'conv'
    elsif action == "poop" || "Poop"
      poop_conv
      return 'conv'
    elsif action == "go elsewhere" || "Go elsewhere" || "Go Elsewhere"
      go_conv
      return 'conv'
    elsif action == "think" || "Think"
      think_conv
      return 'conv'
    elsif action == "talk" || "Talk"
      talk_conv
      return 'conv_fight'
    else
      insults
      return 'conv'
    end
  end
end

class Convefight < Room
  
  def enter()
  
    iconvfight
    print "> "
    
    action = $stdin.gets.chomp
    
    if action == "fight" || "Fight"
      fight_conv
      conv_fight = Puzz.new()
      conv_fight.time = 5
      conv_fight.prompt = "[Beatdown]> "
      conv_fight.dialog_1 =
      conv_fight.dialog_2 =
      conv_fight.dialog_3 =
      conv_fight.pass = pass_conv
      conv_fight.fail = fail_conv
      conv_fight.room_1 = 'fishing'
      conv_fight.room_2 = 'men_brkd'
      conv_fight.comb_room()
    elsif action == "think" || "Think"
      think_conv
      return 'conv_fight'
    else
      puts "You need to [think] about the situation you are in right now."
      sleep 2
      return 'conv_fight'
    end
  end
end

class Fishing < Room
  
  
  def enter()
    
    ifishing
    slp
    puts "CONGLATURATIONS! YOU JUST FISHED!"
    slp
    puts "YOU HAVE LIVED A DAY IN MILES SHOES!"
    slp
    puts "THANK YOU FOR PLAYING GAME<!"
    sleep 6
  end
end

class Map
  @@rooms = {
    'house' => House.new(),
    'outside' => Outside.new(),
    'garage' => Garage.new(),
    'chrismar' => Chrismar.new(),
    'chris_fight' => Chrisfight.new(),
    'conv' => Conve.new(),
    'conv_fight' => Convefight.new(),
    'fishing' => Fishing.new(),
    'men_brkd' => Men_brk.new(),
  }
  
  def initialize(start_room)
    @start_room = start_room
  end
  
  def next_room(room_name)
    val = @@rooms[room_name]
    return val
  end
  
  def opening_room()
    return next_room(@start_room)
  end
end

a_map = Map.new('house')
a_game = Engine.new(a_map)
puts "At any time when prompted by '>' you may type 'think' to check what actions are available."
a_game.play()
