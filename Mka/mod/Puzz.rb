module Puzz
  #defines Dialog for both fights
  
  #defines the prompt title ([Dial]> )
  #the comments 1-3
  #allows for the pass/fail string to be set
  #as well as room 1 or 2
  #
  attr_accessor :prompt, :dialog_1, :dialog_2, :dialog_3, :pass, :fail, :room_1, :room_2, :time
  
  #define combination room (comb_room)
  def comb_room()
    
    #code is set to random 1-3
    code = "#{rand(1..3)}#{rand(1..3)}#{rand(1..3)}"
    #calls prmpt string
    print @prompt
    #sets attempt to input
    attempt = $stdin.gets.chomp
    #sets attempt to 0 
    attempts = 0
    #
    #While loop
    #if the input does not equal the code and the attempts are less than 10
    #puts diag1-3 and adds 1 to the attempts counter
    #then asks for the input again inside the while loop
    while attempt != code && attempts < time
      puts @dialog_1
      sleep 2
      puts @dialog_2
      sleep 2
      puts @dialog_3
      attempts += 1
      print @prompt
      attempt = $stdin.gets.chomp
    end
    
    #When the while loop is ended due to the conditions above
    #it runs the next block
    #If the input is set to the code
    #it runs the pass string and returns it to the set room
    #else if the input is not equal to the code
    #it runs the fail string
    #and returns you to the room
    if attempt == code
      @pass
      return @room_1
    else
      @fail
      return @room_2
    end
  
  end
end
